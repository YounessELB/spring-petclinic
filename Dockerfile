FROM openjdk:8-jdk-alpine

VOLUME /tmp
ADD target/spring-petclinic*.jar /app.jar
CMD ["java", "-jar", "/app.jar"]
EXPOSE 8080